/* Construa uma modelagem estruturada e em seguida sua programa��o estruturada do seguinte problema:

Uma empresa de desenvolvimento de software possui controle de seus produton�rios, clientes e produtos desenvolvidos. Cada controle permite:
 - Cadastro;
 - Consulta total e por um campo escolhido;
 - Altera��o;
 - Exclus�o de registro;
 - Exclus�o total do arquivo.
 
No arquivo produton�rios: entrar com sal�rio bruto, v�o calcular e armazenar sal�rio l�quido, INSS.
No arquivo Clientes: Clientes com 10 ou mais anos de fidelidade 50% de desconto nos produtos.
No arquivo Produtos: Vers�o do produto com data; superior a 3 anos, mensagem "realizar atualiza��o de vers�o". */

#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<ctype.h>
#include<io.h>
#include<string.h>
#include<dos.h>
#include<time.h>

using namespace std;

struct dados
{
	char nome[50];
	char endereco[50];
	int fone;
	char cnpj[30];
	char email[30];
	char dtcontrato[10];
	int anodcontrato;
} cliente;

FILE *client;
FILE *clientnovo;
char op;
int tam, opc;

void cria_arq_client() {
	if ((client = fopen("clientes.dat","a"))==NULL) {
		printf("Erro de criacao do arquivo");
		return;
	}
	printf("Arquivo Criado");
	//delay(10000);
	fclose(client);
}

void cria_arq_client1() {
	if ((clientnovo = fopen("novo_client.dat","a"))==NULL) {
		printf("Erro de criacao do arquivo");
		return;
	}
	printf("Arquivo Criado");
	//delay(10000);
	fclose(clientnovo);
}

void cadastrar_client() {
	system("cls");
	int tamanho=0;
	client = fopen("clientes.dat","r+");
	rewind(client);//rewind retorna pro in�cio do arquivo
	do {
		fread(&cliente,sizeof(cliente),1,client);
		tamanho++;
	} while(!feof(client));

	fseek(client,sizeof(cliente),tamanho);//fseek aponta para a posi��o
	do {
		system("cls");
		printf("\nDigite o nome do cliente:");
		cin>>cliente.nome;
		fflush(stdin);
		printf("Digite o endereco:");
		gets(cliente.endereco);
		fflush(stdin);
		printf("Digite o telefone:");
		cin>>cliente.fone;
		printf("Digite o cnpj:");
		cin>>cliente.cnpj;
		printf("Digite o e-mail:");
		cin>>cliente.email;
		printf("Digite a data do in�cio de contrato:");
		cin>>cliente.dtcontrato;
		printf("Digite o ano em que foi estabelecido contrato:");
		cin>>cliente.anodcontrato;
		if(cliente.anodcontrato<2007){
			printf("\n\nESTE CLIENTE RECEBE 50 POR CENTO DE DESCONTO EM TODOS OS PRODUTOS.\n\n");
		}
		tamanho=tamanho+fwrite(&cliente,sizeof(struct dados),1,client);
		printf("\nContinuar cadastrando <s> ou <n>:");
		cin>>op;
		op = toupper(op);
	} while (op=='S');
	fclose(client);
}

void listar_client() {
    system("cls");
	client=fopen("clientes.dat","r+");
	fseek(client,sizeof(struct dados),0);

	while(fread(&cliente,sizeof(cliente),1,client)==1) {
		system("cls");
		printf("Nome: %s\n", cliente.nome);
		printf("Endereco: %s\n", cliente.endereco);
		printf("Telefone: %d\n", cliente.fone);
		printf("CNPJ: %s\n", cliente.cnpj);
		printf("E-mail: %s\n", cliente.email);
		printf("Data de in�cio de contrato: %s/%d\n", cliente.dtcontrato, cliente.anodcontrato);
		printf("\nDigite enter para continuar\n");
		getch();
	}
	printf("fim do arquivo");
	fclose(client);
	getch();
}

void consultar_nome_client() {
	char nom_pro[50];//igual ao char nome[20] da struct dados
	int achou,
	    cont=0;
	system("cls");
	client=fopen("clientes.dat","r+");
	fseek(client,sizeof(struct dados),0);
	printf("Digite o Nome p/ Procura: ");
	cin>> nom_pro;
	while(fread(&cliente,sizeof(cliente),1,client)==1) {
		achou=strcmp(cliente.nome,nom_pro);
		if(achou==0) {
		printf("Nome: %s\n", cliente.nome);
		printf("Endereco: %s\n", cliente.endereco);
		printf("Telefone: %d\n", cliente.fone);
		printf("CNPJ: %s\n", cliente.cnpj);
		printf("E-mail: %s\n", cliente.email);
		printf("Data de in�cio de contrato: %s/%d\n", cliente.dtcontrato, cliente.anodcontrato);
		printf("\nDigite enter para continuar\n");
		getch();
			cont=1;
		}
	}
}

void consulta_cnpj_client() {
	char cnpj_pro[15];
	int achou,
	    cont=0;
	system("cls");
	client=fopen("clientes.dat","r+");
	fseek(client,sizeof(struct dados),0);
	printf("Digite o cnpj p/ Procura: ");
	cin>> cnpj_pro;
	while(fread(&cliente,sizeof(cliente),1,client)==1) {
		achou=strcmp(cliente.cnpj,cnpj_pro);
		if(achou==0) {
		printf("Nome: %s\n", cliente.nome);
		printf("Endereco: %s\n", cliente.endereco);
		printf("Telefone: %d\n", cliente.fone);
		printf("CNPJ: %s\n", cliente.cnpj);
		printf("E-mail: %s\n", cliente.email);
		printf("Data de in�cio de contrato: %s/%d\n", cliente.dtcontrato, cliente.anodcontrato);
		printf("\nDigite enter para continuar\n");
		getch();
		cont=1;
	}
  }
}

void menu_consultar_client() {
	system("cls");
    printf("1 - Listar clientes\n");
	printf("2 - Consultar por nome\n");
	printf("3 - Consultar por CNPJ\n");
	printf("4 - Voltar\n");
	cin>>opc;
	switch(opc) {
		case 1:
			listar_client();
			break;
		case 2:
			consultar_nome_client();
			break;
		case 3:
			consulta_cnpj_client();
			break;
		case 4:
			return;			
	}
}			
	
	
void alterar_client(){
     char nom_pro[50];
     int achou;
     int pos=0;
	system("cls");
	client=fopen("clientes.dat","r+");
	fseek(client,sizeof(struct dados),0);
	printf("Digite o nome do cliente a ser alterado:");
	cin>> nom_pro;
	while(fread(&cliente,sizeof(cliente),1,client)==1){
		achou=strcmp(cliente.nome, nom_pro);
		pos++;
		if(achou==0){
		// fseek(fp,cont*sizeof(struct dados),SEEK_SET);
		printf("Nome: %s\n",cliente.nome);
		printf("Endereco: %s\n",cliente.endereco);
		printf("Telefone: %d\n",cliente.fone);
		printf("CNPJ: %s\n",cliente.cnpj);
		printf("E-mail: %s\n",cliente.email);
		printf("Data do inicio do contrato: %s/%d\n",cliente.dtcontrato, cliente.anodcontrato);
		printf("_______________\n");
		getch();
		printf("\nDigite o nome do cliente:");
     	cin>>cliente.nome;
   		fflush(stdin);
   		printf("\nDigite o endereco:");
	  	gets(cliente.endereco);
     	fflush(stdin);
     	printf("\nDigite o fone:");
     	cin>>cliente.fone;
     	printf("\nDigite o CNPJ:");
     	cin>>cliente.cnpj;
     	printf("\nDigite o email:");
     	cin>>cliente.email;
     	fflush(stdin);
     	printf("\nDigite a data de inicio de contrato:");
	    cin>>cliente.dtcontrato;
	    printf("\nDigite o ano em que foi estabelecido o contrato:");
	    cin>>cliente.anodcontrato;
	fseek(client,pos*sizeof(struct dados),SEEK_SET);
	fwrite(&cliente,sizeof(struct dados),1,client);
	printf("\nDigite enter para continuar\n");
	getch();}}
	fclose(client);
}
		
void excluir_client()
{
    char nom_pro[50];
    int achou;
	int tamanhon=0,tamanhoc=0;
	system("cls");
	client=fopen("clientes.dat","r+");
	fseek(client,sizeof(struct dados),0);
	cria_arq_client1();
	clientnovo=fopen("novo_client.dat","r+");
	fseek(clientnovo,sizeof(struct dados),0);

	printf("Digite o Nome p/ Procura : ");
	cin>> nom_pro;
	       while(fread(&cliente,sizeof(cliente),1,client)==1){
		achou=strcmp(cliente.nome,nom_pro);
		if(achou==0){
		printf("Nome: %s\n",cliente.nome);
		printf("Endereco: %s\n",cliente.endereco);
		printf("Telefone: %d\n",cliente.fone);
		printf("CNPJ: %s\n",cliente.cnpj);
		printf("E-mail: %s\n",cliente.email);
		printf("Data do inicio do contrato: %s/%d\n",cliente.dtcontrato, cliente.anodcontrato);
		printf("_______________\n");
		getch();
		}
		else{
	    tamanhon=tamanhon+fwrite(&cliente,sizeof(struct dados),1,client);}}
        fclose(client);
	    remove("clientes.dat");
	    cria_arq_client1();
	    fseek(clientnovo,sizeof(struct dados),0);
	    client=fopen("clientes.dat","r+");
	    fseek(client,sizeof(struct dados),0);
	while(fread(&cliente,sizeof(cliente),1,clientnovo)==1) {
	tamanhoc=tamanhoc+fwrite(&cliente,sizeof(struct dados),1,client);}
	fclose(clientnovo);
	remove("novo_client.dat");
	fclose(client);
}

void excluir_arq_client(){
    char opex;
    printf("Deleta Arquivo <s> ou <n>? ");
    cin>>opex;
    if (opex == 's')  {
      remove("clientes.dat"); 
      printf("arquivo deletado");}
    else
      printf(" arquivo n�o foi deletado por sua op��o");
    getch();
}

void menu_excluir_client() {
	system("cls");
    printf("1 - Excluir um cliente\n");
	printf("2 - Excluir todos os clientes\n");
	printf("3 - Voltar\n");
	cin>>opc;
	switch(opc) {
		case 1:
			excluir_client();
			break;
		case 2:
			excluir_arq_client();
			break;
		case 3:
			return;			
	}
}		

void menu_client() {
	system("cls");
	printf("Menu de opcoes");
	printf("\n");
	printf("1 - Cadastrar clientes\n");
	printf("2 - Consultar clientes\n");
	printf("3 - Alterar clientes\n");
	printf("4 - Excluir clientes\n");
	printf("5 - Sair\n");
	printf("Digite uma opcao\n");
	cin>>opc;
	switch(opc) {
		case 1:
			cadastrar_client();
			break;
		case 2:
			menu_consultar_client();
			break;
		case 3:
			alterar_client();
			break;
		case 4:
			menu_excluir_client();
			break;		
		case 5:
			exit(0);			
	}
}

main() {
	system("cls");
	cria_arq_client();
	opc = 1;
	do {
		menu_client();
	} while (op!=5);
}

